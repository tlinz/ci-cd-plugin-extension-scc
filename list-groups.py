import gitlab
import json
import csv
import os
import sys
import argparse

def get_group_projects(gl, group):
    repo_urls = []
    group_object = gl.groups.get(group)
    group_projects = group_object.projects.list(as_list=False, include_subgroups=True)
    for group_project in group_projects:
        project_object = gl.projects.get(group_project.id)
        repo_urls.append(project_object.attributes["http_url_to_repo"])
    return repo_urls

parser = argparse.ArgumentParser(description='Check for data leak in a GitLab.com group')
parser.add_argument('token', help='API token able to read the requested group')
parser.add_argument('group', help='The group to run this report on')
parser.add_argument('--visibility', '-v', help='Run visibility report', action='store_true')
parser.add_argument('--forks', '-f', help='Run visibility report', action='store_true')
parser.add_argument('--projectshares', '-p', help='Run project share report', action='store_true')
args = parser.parse_args()

gitlaburl = "https://gitlab.com/"
gl = gitlab.Gitlab(gitlaburl, private_token=args.token)

group = args.group
repo_urls = []

repo_urls = get_group_projects(gl, group)
repo_string = "|".join(repo_urls)
print(repo_string)
